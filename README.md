# ASTERISK

## POUR :
Les entreprises de petite à moyenne taille, les organisations éducatives et les groupes de travail collaboratif comme Axelor
qui ont besoin d'un système de téléphonie interne efficace et de la capacité à organiser des conférences téléphoniques régulières .

QUI VEULENT :
Les utilisateurs (employés, équipes de support, départements de vente, etc.) veulent un système de communication interne fiable, 
capable de gérer les appels entrants et sortants, de créer des extensions pour chaque employé, 
et d'organiser des réunions téléphoniques multi-participants.

PRODUIT :
Le produit est un serveur PBX (Private Branch Exchange) et de conférence téléphonique basé sur Asterisk un lifociel opensource.

EST UN :
C'est une application serveur de téléphonie IP opensource.

QUI :
Les avantages clés incluent la réduction des coûts de communication, l'amélioration de la communication interne, 
la capacité à organiser des réunions téléphoniques efficaces avec plusieurs participants, 
et la flexibilité d'ajouter des fonctionnalités avancées telles que la messagerie vocale, les conférences téléphoniques et les menus IVR.

À LA DIFFÉRENCE DE :
Les principales alternatives incluent les systèmes téléphoniques traditionnels basés sur du matériel propriétaire, 
les services VoIP cloud, et les services de conférence téléphonique payants comme Zoom, Microsoft Teams, et Cisco WebEx, 
qui peuvent être coûteux et moins flexibles.

NOTRE PRODUIT :
Notre solution Asterisk se différencie par son coût réduit, sa flexibilité et sa personnalisation. 


### planification des taches 
Estimer combien vous sentez-vous capable de faire de points durant le premier Sprint (capacité)
 nous estimons à 16 par jour notre capacité

Estimer le durée du projet : nombre total de points / nombre de points par jour = nombre de jour

Estimer le durée du projet : 31 / 16 = 2 jours 

#### Potentiels Risques pour le Projet Asterisk
Risque de Sécurité du Réseau

Description : Une faille de sécurité pourrait permettre un accès non autorisé au serveur Asterisk.
Gravité : POURRAIT RÉSULTER EN CATASTROPHE
Vraisemblance : POSSIBLE
Évaluation : EXTRÊME (3 – Intolérable)
Plan d'Action : Mettre en place des pare-feu, utiliser Fail2Ban, et surveiller les journaux de sécurité en continu.
Risque de Configuration Incorrecte

Description : Une mauvaise configuration pourrait entraîner des dysfonctionnements du système PBX ou des conférences.
Gravité : IMPACT GRAVE
Vraisemblance : POSSIBLE
Évaluation : HAUT (2 – Généralement inacceptable)
Plan d'Action : Effectuer des tests rigoureux, vérifier les configurations plusieurs fois, et former les administrateurs.
Risque de Panne Matérielle

Description : Une défaillance du serveur pourrait interrompre les services de téléphonie.
Gravité : IMPACT GRAVE
Vraisemblance : IMPROBABLE
Évaluation : DOULEUR MOYENNE (1 – ALARP)
Plan d'Action : Utiliser des unités d'alimentation sans interruption (UPS) et mettre en place des sauvegardes régulières.
Risque de Charge de Travail Élevée

Description : Un volume d'appels élevé pourrait surcharger le serveur et dégrader les performances.
Gravité : LES EFFETS SE FONT SENTIR
Vraisemblance : PROBABLE
Évaluation : HAUT (2 – Généralement inacceptable)
Plan d'Action : Optimiser les configurations de performance et surveiller la charge en temps réel.
Risque de Non-Compatibilité avec les Softphones

Description : Les softphones sur Android peuvent rencontrer des problèmes de compatibilité avec Asterisk.
Gravité : LES EFFETS SE FONT SENTIR
Vraisemblance : POSSIBLE
Évaluation : DOULEUR MOYENNE (1 – ALARP)
Plan d'Action : Tester différents softphones et fournir des recommandations basées sur les résultats.

##### QUESTION : 

Comment appelle-t-on un mini-projet dans Scrum ?
Dans Scrum, on appelle un mini-projet un "Sprint".

Que représente le mot scrum ?
Le mot "scrum" fait référence à une méthodologie de gestion de projet itérative et agile, basée sur des cycles de développement courts et itératifs appelés "Sprints".

Qu’est-ce qu’une User Story ?
Une User Story est une description simple et concise d'une fonctionnalité logicielle du point de vue de l'utilisateur final. Elle est généralement rédigée sous la forme d'une phrase courte qui décrit le qui, quoi et pourquoi de la fonctionnalité.

Qu’est-ce que le backlog ?
Le backlog, également appelé "Product Backlog", est une liste dynamique et priorisée de toutes les fonctionnalités, améliorations et corrections de bogues attendues pour un produit logiciel. Il représente les exigences du produit et est constamment mis à jour tout au long du projet.

Les Issues font-elles partie de la méthode Scrum ?
Les Issues sont généralement associées à des systèmes de suivi de problèmes tels que GitHub ou Jira, mais elles ne font pas partie intégrante de la méthodologie Scrum. Cependant, elles peuvent être utilisées en parallèle avec Scrum pour gérer les problèmes et les tâches non liées au développement logiciel.

Comment estimer la durée d’un projet ?
La durée d'un projet peut être estimée en utilisant des techniques telles que l'estimation par points de story, la planification du sprint, les réunions de rétrospective et l'expérience passée. Dans Scrum, l'estimation se fait généralement en mesurant la complexité des User Stories à l'aide de points de story, puis en prévoyant la capacité de travail de l'équipe pour chaque sprint.

Quelle est la différence entre une User Story et une tâche ?
Une User Story est une description de haut niveau d'une fonctionnalité du point de vue de l'utilisateur final, tandis qu'une tâche est une action spécifique nécessaire pour réaliser cette fonctionnalité. Les tâches sont généralement des sous-éléments des User Stories et représentent les actions concrètes que les membres de l'équipe doivent effectuer pour implémenter la fonctionnalité.